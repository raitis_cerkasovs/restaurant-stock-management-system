# Restaurant stock management system #



To clone:

   *> git clone https://raitis_cerkasovs@bitbucket.org/raitis_cerkasovs/restaurant-stock-management-system.git*

run from SBT:

   *> cd restaurant-stock-management-system*

   *> sbt run*


## Initial Data ##


 var databank = Vector\[Item\](
    Item("spoon", 1.21, "cutlery", 12),
    Item("cake", 3.23, "desert", 32),
    Item("carrot", 1.23, "vegetable", 2),
    Item("onion", 2.23, "vegetable", 6),
    Item("plum", 0.34, "fruit", 5)
  )




## Comands ##


all by category (vegetables):

  > curl -v http://localhost:5000/allitems/vegetable

all running out:

  > curl -v http://localhost:5000/allitemsruningout

add item:

  > curl -v -H "Content-Type: application/json" \
     -X POST http://localhost:5000/additem \
     -d '{"name": "apple", "price": 1.02, "category": "fruit", "quantity": 45}'

remove item:

  > curl -v -X DELETE http://localhost:5000/item/spoon

add quantity:

  > curl -v -H "Content-Type: application/json" \
     -X POST http://localhost:5000/item \
     -d '{"name": "plum", "price": 0.34, "category": "fruit", "quantity": 45}'

or deduct:

  > curl -v -H "Content-Type: application/json" \
     -X POST http://localhost:5000/item \
     -d '{"name": "plum", "price": 0.34, "category": "fruit", "quantity": -3}'