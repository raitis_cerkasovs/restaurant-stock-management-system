package com.exercise

/**
  * Created by raitis on 19/03/2016.
  */

import akka.actor._
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import spray.can.Http

import scala.concurrent.duration._


object Main extends App {

  // read config
  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

  // must be same with config -> spray server header
  implicit val system = ActorSystem("API-service")

  val api = system.actorOf(Props(new RestInterface()), "httpInterface")

  // context
  implicit val executionContext = system.dispatcher
  implicit val timeout = Timeout(10 seconds)

  // bind to Http
  IO(Http).ask(Http.Bind(listener = api, interface = host, port = port))
    .mapTo[Http.Event]
    .map {
      case Http.Bound(address) =>
        println(s"REST interface bound to $address")
      case Http.CommandFailed(cmd) =>
        println("REST interface could not bind to " +
          s"$host:$port, ${cmd.failureMessage}")
        system.shutdown()
    }

}

