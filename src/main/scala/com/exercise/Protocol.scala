package com.exercise

/**
  * Created by raitis on 19/03/2016.
  */

import spray.json._


object Protocol {

  case class Item(name: String, price: Double, category: String, quantity: Int)

  //json (un)marshalling
  object Item extends DefaultJsonProtocol {
    implicit val format = jsonFormat4(Item.apply)
  }

  // responders
  case object ItemCreated
  case object ItemAlreadyExists
  case object ItemDeleted
  case object ItemUpdated
  case object ItemNotFound

}
