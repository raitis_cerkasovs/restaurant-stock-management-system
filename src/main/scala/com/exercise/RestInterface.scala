package com.exercise

/**
  * Created by raitis on 19/03/2016.
  */

import akka.actor._
import akka.util.Timeout
import com.exercise.Protocol._
import spray.http.StatusCodes
import spray.httpx.SprayJsonSupport.sprayJsonUnmarshaller
import spray.routing._
import spray.json._

import scala.concurrent.duration._
import scala.language.postfixOps


/**
  *  Http Service class
  */
class RestInterface extends HttpServiceActor with RestApi {
  def receive = runRoute(routes)
}



/**
  *  main Trait
  */
trait RestApi extends HttpService with ActorLogging {
  actor: Actor =>

  implicit val timeout = Timeout(10 seconds)

  private def createResponder(requestContext: RequestContext) = {
    context.actorOf(Props(new Responder(requestContext)))
  }

  /**
    *  Routes
    *
    * @return
    */
  def routes: Route =

    pathPrefix("additem") {
      post {
        entity(as[Item]) { item => requestContext =>
          val responder = createResponder(requestContext)
          addItem(item) match {
            case true => responder ! ItemCreated
            case _ => responder ! ItemAlreadyExists
          }
        }
      }
    } ~
      pathPrefix("allitems") {
        path(Segment) { category =>
          get {
            complete {
              getAllItems(category)
            }
          }
        }
      } ~
      pathPrefix("allitemsruningout") {
        pathEnd {
          get {
            complete {
              getAllItemsRuningOut
            }
          }
        }
      } ~
      pathPrefix("item") {
        path(Segment) { name =>
          delete { requestContext =>
            val responder = createResponder(requestContext)
            deleteItem(name)
            responder ! ItemDeleted
          }
        } ~
          post {
            entity(as[Item]) { item => requestContext =>
              val responder = createResponder(requestContext)
              updateItem(item.name, item.quantity) match {
                case true => responder ! ItemUpdated
                case _ => responder ! ItemNotFound
              }
            }
          }
        }


  /**
    * DAO
    */

  // initial data
  var databank = Vector[Item](
    Item("spoon", 1.21, "cutlery", 12),
    Item("cake", 3.23, "desert", 32),
    Item("carrot", 1.23, "vegetable", 2),
    Item("onion", 2.23, "vegetable", 6),
    Item("plum", 0.34, "fruit", 5)
  )


  // data access
  private def getAllItems(category: String): String = {
    databank.filter(items => items.category == category).toJson.toString
  }

  private def getAllItemsRuningOut: String = {
    val limit = 10
    databank.filter(items => items.quantity <= limit).toJson.toString
  }

  private def addItem(item: Item): Boolean = {
    val doesNotExist = !databank.exists(_.name == item.name)
    if (doesNotExist) databank = databank :+ item
    doesNotExist
  }

  private def deleteItem(name: String): Unit = {
    databank = databank.filterNot(_.name == name)
  }

  private def updateItem(name: String, quantity: Int): Boolean = {
    val doExist = databank.exists(_.name == name)
    val oldItem = databank.find(_.name == name).get
    deleteItem(name)
    addItem(Item(oldItem.name, oldItem.price, oldItem.category, oldItem.quantity + quantity))
    doExist
  }


}


/**
  * Context responders
  *
  * @param requestContext
  */
class Responder(requestContext:RequestContext) extends Actor with ActorLogging {

  def receive = {

    case ItemCreated =>
      requestContext.complete(StatusCodes.Created)
      killYourself

    case ItemAlreadyExists =>
      requestContext.complete(StatusCodes.Conflict)
      killYourself

    case ItemDeleted =>
      requestContext.complete(StatusCodes.OK)
      killYourself


    case ItemUpdated=>
      requestContext.complete(StatusCodes.OK)
      killYourself

    case ItemNotFound =>
      requestContext.complete(StatusCodes.NotFound)
      killYourself
  }

  private def killYourself = self ! PoisonPill

}